use crate::color::NHPaletteItem;
use std::fmt;
use std::fmt::Write;

const FRAME_STEP: i32 = 16;
const PRESS_FRAMES: i32 = 8;

pub struct TASScript {
    pub script: String,
    pub start_frame: i32,
    pub end_frame: i32,
}

impl TASScript {
    fn add_keypress(&mut self, key: KEY) {
        let _ = writeln!(
            self.script,
            "{}-{} {} 0;0 0;0",
            self.start_frame,
            self.start_frame + PRESS_FRAMES - 1,
            key
        );
        let _ = writeln!(
            self.script,
            "{}-{} {} 0;0 0;0",
            self.start_frame + PRESS_FRAMES,
            self.end_frame,
            KEY::NONE
        );
        self.start_frame = self.start_frame + FRAME_STEP;
        self.end_frame = self.start_frame + FRAME_STEP - 1;
    }

    pub fn add_palette_input(&mut self, palette: &[NHPaletteItem]) {
        // open palette menu
        self.add_keypress(KEY::X);
        self.add_keypress(KEY::DUP);
        self.add_keypress(KEY::DRIGHT);
        self.add_keypress(KEY::A);
        for (index, item) in palette.iter().enumerate() {
            match item {
                NHPaletteItem::Color { h, s, v } => {
                    // Set hue
                    for _ in 0..*h {
                        self.add_keypress(KEY::DRIGHT);
                    }
                    // navigate to vididness
                    self.add_keypress(KEY::DDOWN);
                    // Set saturation
                    for _ in 1..*s {
                        self.add_keypress(KEY::DRIGHT);
                    }
                    // navigate to brightness
                    self.add_keypress(KEY::DDOWN);
                    // Reset brightness to leftmost value
                    for _ in 0..15 - index {
                        self.add_keypress(KEY::DLEFT);
                    }
                    // Set brightness
                    for _ in 1..*v {
                        self.add_keypress(KEY::DRIGHT);
                    }
                    // navigate to next palette item and reselect hue
                    self.add_keypress(KEY::R);
                    self.add_keypress(KEY::DDOWN);
                }
                NHPaletteItem::Transparent => {}
            }
        }
        self.add_keypress(KEY::A);
        self.add_keypress(KEY::DLEFT);
        self.add_keypress(KEY::DDOWN);
        self.add_keypress(KEY::A);
    }

    pub fn add_image_input(&mut self, img: &[u8], dimensions: (usize, usize)) {
        let mut last_color = 0;
        let mut pixel_index: usize;
        for y in 0..dimensions.0 {
            if y % 2 == 0 {
                for x in 0..dimensions.1 {
                    pixel_index = dimensions.0 * y + x;
                    self.add_keypress(KEY::A);
                    last_color = self.add_color_input(last_color, img[pixel_index]);
                    if x != dimensions.0 - 1 {
                        self.add_keypress(KEY::DRIGHT);
                    }
                }
            } else {
                for x in (0..dimensions.1).rev() {
                    pixel_index = dimensions.1 * y + x;
                    self.add_keypress(KEY::A);
                    last_color = self.add_color_input(last_color, img[pixel_index]);
                    if x != 0 {
                        self.add_keypress(KEY::DLEFT);
                    }
                }
            }
            self.add_keypress(KEY::DDOWN);
        }
    }

    fn add_color_input(&mut self, last_color: u8, new_color: u8) -> u8 {
        // TODO: wrap around to reduce key presses
        if last_color > new_color {
            for _ in 0..last_color - new_color {
                self.add_keypress(KEY::L);
            }
        }
        if last_color < new_color {
            for _ in 0..new_color - last_color {
                self.add_keypress(KEY::R);
            }
        }
        //last_color == new_color
        self.add_keypress(KEY::A);
        new_color
    }
}

#[allow(dead_code)]
enum KEY {
    A,
    B,
    X,
    Y,
    LSTICK,
    RSTICK,
    L,
    R,
    ZL,
    ZR,
    PLUS,
    MINUS,
    DLEFT,
    DUP,
    DRIGHT,
    DDOWN,
    NONE,
}

impl fmt::Display for KEY {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            KEY::A => write!(f, "{}", "KEY_A"),
            KEY::B => write!(f, "{}", "KEY_B"),
            KEY::X => write!(f, "{}", "KEY_X"),
            KEY::Y => write!(f, "{}", "KEY_Y"),
            KEY::LSTICK => write!(f, "{}", "KEY_LSTICK"),
            KEY::RSTICK => write!(f, "{}", "KEY_RSTICK"),
            KEY::L => write!(f, "{}", "KEY_L"),
            KEY::R => write!(f, "{}", "KEY_R"),
            KEY::ZL => write!(f, "{}", "KEY_ZL"),
            KEY::ZR => write!(f, "{}", "KEY_ZR"),
            KEY::PLUS => write!(f, "{}", "KEY_PLUS"),
            KEY::MINUS => write!(f, "{}", "KEY_MINUS"),
            KEY::DLEFT => write!(f, "{}", "KEY_DLEFT"),
            KEY::DUP => write!(f, "{}", "KEY_DUP"),
            KEY::DRIGHT => write!(f, "{}", "KEY_DRIGHT"),
            KEY::DDOWN => write!(f, "{}", "KEY_DDOWN"),
            KEY::NONE => write!(f, "{}", "NONE"),
        }
    }
}
