pub mod color;
pub mod design;
pub mod script;
use color::HSVRGBA;
use design::Design;
use exoquant::optimizer;
use exoquant::ditherer;

use script::TASScript;
use js_sys::Array;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{Blob, HtmlAnchorElement, Url};

#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just
    // `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

// Called when the wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    document.body().expect("document should have a body");

    Ok(())
}

#[wasm_bindgen]
pub fn design_new() -> *mut Design {
    Box::into_raw(Box::new(Design::default()))
}

#[wasm_bindgen]
pub fn design_palette(design: *const Design) -> Result<JsValue, JsValue> {
    if let Some(design) = unsafe { design.as_ref() } {
        let items: Vec<HSVRGBA> = design.palette().iter().map(Into::into).collect();

        JsValue::from_serde(&items).map_err(|e| e.to_string().into())
    } else {
        Err("Got a null pointer for design".into())
    }
}

#[wasm_bindgen]
pub fn design_dimensions(design: *const Design) -> Result<JsValue, JsValue> {
    if let Some(design) = unsafe { design.as_ref() } {
        JsValue::from_serde(&design.dimensions()).map_err(|e| e.to_string().into())
    } else {
        Err("Got a null pointer for design".into())
    }
}

#[wasm_bindgen]
pub fn design_generate(design: *const Design, ditherer: String) -> Result<JsValue, JsValue> {
    if let Some(design) = unsafe { design.as_ref() } {
        let data = match ditherer.as_str() {
            "none" => design.generate(ditherer::None),
            "floydsteinberg" => design.generate(ditherer::FloydSteinberg::new()),
            "floydsteinbergvanilla" => design.generate(ditherer::FloydSteinberg::vanilla()),
            "floydsteinbergcheckered" => design.generate(ditherer::FloydSteinberg::checkered()),
            "ordered" => design.generate(ditherer::Ordered),
            e => return Err(format!("ditherer {} not recognized", e).into()),
        };

        let window = web_sys::window().expect("no global `window` exists");
        let document = window.document().expect("should have a document on window");
        let download_element = document
            .get_element_by_id("download_script")
            .expect("should have download element")
            .dyn_into::<HtmlAnchorElement>()
            .expect("#script should be an `HtmlAnchorElement`");
        let text = Array::new();

        let mut x = TASScript {
            start_frame: 1,
            end_frame: 15,
            script: String::new(),
        };
        x.add_palette_input(design.palette());
        x.add_image_input(&data, design.dimensions());

        text.push(&x.script.into());
        let blob = Blob::new_with_str_sequence(&text.into()).unwrap();
        let url = Url::create_object_url_with_blob(&blob).expect("should be a url");
        download_element.set_href(&url);
        download_element.set_download("tas_script.txt");
        
        JsValue::from_serde(&data).map_err(|e| e.to_string().into())
    } else {
        Err("Got a null pointer for design".into())
    }
}

#[wasm_bindgen]
pub fn design_load_palette(design: *mut Design, buffers: JsValue) -> Result<(), JsValue> {
    if let Some(design) = unsafe { design.as_mut() } {
        let array: js_sys::Array = buffers.into();
        let buffers: Vec<js_sys::Uint8ClampedArray> = array.to_vec().into_iter().map(Into::into).collect();
        let data: Vec<Vec<(u8, u8, u8, u8)>> = buffers.into_iter().map(|b| b.to_vec().chunks_exact(4).map(|slice| (slice[0], slice[1], slice[2], slice[3])).collect()).collect();
        design.load_histogram(data).map_err(Into::into)
    } else {
        Err("Got a null pointer for design".into())
    }
}

#[wasm_bindgen]
pub fn design_optimize_palette(design: *mut Design, optimizer: String) -> Result<(), JsValue> {
    if let Some(design) = unsafe { design.as_mut() } {
        match optimizer.as_str() {
            "kmeans" => design.optimize_palette(optimizer::KMeans),
            "weightedkmeans" => design.optimize_palette(optimizer::WeightedKMeans),
            e => return Err(format!("optimizer {} not recognized", e).into()),
        }
        Ok(())
    } else {
        Err("Got a null pointer for design".into())
    }
}

#[wasm_bindgen]
pub fn design_load_image(design: *mut Design, buffer: JsValue, width: u32, height: u32) -> Result<(), JsValue> {
    if let Some(design) = unsafe { design.as_mut() } {
        let buffer: js_sys::Uint8ClampedArray = buffer.into();
        let data: Vec<(u8, u8, u8, u8)> = buffer.to_vec().chunks_exact(4).map(|slice| (slice[0], slice[1], slice[2], slice[3])).collect();
        design.load_image(data, (width as usize, height as usize)).map_err(Into::into)
    } else {
        Err("Got a null pointer for design".into())
    }
}
